function bingMapsReady() {
    Microsoft.Maps.loadModule("Microsoft.Maps.AutoSuggest", {
        callback: onLoad,
        errorCallback: logError,
        credentials: 'AphLrLUWMuwT-NQrnPJra2UmGokYnUrePttpgb38D1bEbtXwk3joOrWxY0VfpUzu&callback'
    });

    function onLoad() {
        var options = { maxResults: 8 };
        initAutosuggestControl(options, "searchBox", "searchBoxContainer");
        initAutosuggestControl(options, "searchBoxAlt", "searchBoxContainerAlt");
    }
}

function initAutosuggestControl(
    options,
    suggestionBoxId,
    suggestionContainerId
) {
    var manager = new Microsoft.Maps.AutosuggestManager(options);
    manager.attachAutosuggest(
        "#" + suggestionBoxId,
        "#" + suggestionContainerId,
        selectedSuggestion
    );

    function selectedSuggestion(suggestionResult) {
        document.getElementById(suggestionBoxId).innerHTML =
            suggestionResult.formattedSuggestion;
    }
}


function logError(message) {
    console.log(message);
}
