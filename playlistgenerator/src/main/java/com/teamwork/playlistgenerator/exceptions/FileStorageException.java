package com.teamwork.playlistgenerator.exceptions;


public class FileStorageException extends RuntimeException {
    public FileStorageException(String message) {
        super((message));
    }
}
