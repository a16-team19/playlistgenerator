package com.teamwork.playlistgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class PlaylistgeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlaylistgeneratorApplication.class, args);
    }

}
