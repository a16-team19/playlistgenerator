package com.teamwork.playlistgenerator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class TravelPoints {

    private String from;
    private String to;
}
