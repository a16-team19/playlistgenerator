package com.teamwork.playlistgenerator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "users")
public class User {

    public static final String PASSWORD_IS_REQUIRED = "Password is required";
    public static final String USERNAME_IS_REQUIRED = "Username is required";
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id;

    @Size(min = 1, message = USERNAME_IS_REQUIRED)
    @NotNull
    @Column(name = "username")
    private String username;

    @Size(min = 1, message = PASSWORD_IS_REQUIRED)
    @NotNull
    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private Integer enabled;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name = "user_details_id")
    private UserDetails userDetails;

}
