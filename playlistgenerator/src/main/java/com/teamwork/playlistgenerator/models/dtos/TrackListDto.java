package com.teamwork.playlistgenerator.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.teamwork.playlistgenerator.models.Track;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackListDto {
    private List<Track> tracks;
    private long total;
    private String next;

    @JsonProperty("data")
    public List<Track> getTracks() {
        return tracks;
    }

    @JsonProperty("data")
    public void setData(List<Track> tracks) {
        this.tracks = tracks;
    }

    @JsonProperty("total")
    public long getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(long value) {
        this.total = value;
    }

    @JsonProperty("next")
    public String getNext() {
        return next;
    }

    @JsonProperty("next")
    public void setNext(String value) {
        this.next = value;
    }
}
