package com.teamwork.playlistgenerator.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TravelDurationDto {

    private List<ResourceSetDto> resourceSets;
    private long statusCode;

    @JsonProperty("resourceSets")
    public List<ResourceSetDto> getResourceSets() {
        return resourceSets;
    }

    @JsonProperty("resourceSets")
    public void setResourceSets(List<ResourceSetDto> value) {
        this.resourceSets = value;
    }

    @JsonProperty("statusCode")
    public long getStatusCode() { return statusCode; }

    @JsonProperty("statusCode")
    public void setStatusCode(long value) { this.statusCode = value; }

}
