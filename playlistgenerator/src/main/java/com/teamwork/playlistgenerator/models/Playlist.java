package com.teamwork.playlistgenerator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "playlists")
public class Playlist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "playlist_id")
    private Integer id;

    @NotNull
    @Column(name = "name")
    private String title;

    @NotNull
    @Column(name = "duration")
    private Integer duration;

    @Column(name = "user_id")
    private int userId;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "playlists_tracks",
            joinColumns = @JoinColumn(name = "playlist_id"),
            inverseJoinColumns = @JoinColumn(name = "track_id")
    )
    private List<Track> tracklist;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "playlists_genres",
            joinColumns = @JoinColumn(name = "playlists_playlist_id"),
            inverseJoinColumns = @JoinColumn(name = "genres_genre_id")
    )
    private List<Genre> genreList;

}
