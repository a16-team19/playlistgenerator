package com.teamwork.playlistgenerator.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoDto {

    private String username;

    private String email;

    private String firstName;

    private String lastName;

    private String gender;

    private String pictureURL;
}
