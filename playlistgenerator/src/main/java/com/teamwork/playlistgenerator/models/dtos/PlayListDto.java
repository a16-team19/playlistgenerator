package com.teamwork.playlistgenerator.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayListDto {

    private List<TrackListLinkDto> tracksList;
    private long total;
    private String next;

    @JsonProperty("data")
    public List<TrackListLinkDto> getTracksList() {
        return tracksList;
    }

    @JsonProperty("data")
    public void setTracksList(List<TrackListLinkDto> tracksList) {
        this.tracksList = tracksList;
    }

    @JsonProperty("total")
    public long getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(long total) {
        this.total = total;
    }

    @JsonProperty("next")
    public String getNext() {
        return next;
    }

    @JsonProperty("next")
    public void setNext(String next) {
        this.next = next;
    }
}
