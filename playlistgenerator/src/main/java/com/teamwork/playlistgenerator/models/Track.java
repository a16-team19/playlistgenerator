package com.teamwork.playlistgenerator.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@Entity
@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Table(name = "tracks")
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "track_id")
    private BigInteger id;

    @NotNull
    @Column(name = "title")
    private String title;

    @NotNull
    @Column(name = "link")
    private String link;

    @NotNull
    @Column(name = "ranked")
    private Integer rank;

    @NotNull
    @Column(name = "preview_url")
    private String preview;

    @Column(name = "duration")
    Integer duration;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "artist_id")
    private Artist artist;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "album_id")
    private Album album;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "genre_id")
    private Genre genre;
}

