package com.teamwork.playlistgenerator.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceSetDto {

    private long estimatedTotal;
    private List<ResourceDto> resources;

    @JsonProperty("estimatedTotal")
    public long getEstimatedTotal() {
        return estimatedTotal;
    }

    @JsonProperty("estimatedTotal")
    public void setEstimatedTotal(long value) {
        this.estimatedTotal = value;
    }


    @JsonProperty("resources")
    public List<ResourceDto> getResources() {
        return resources;
    }

    @JsonProperty("resources")
    public void setResources(List<ResourceDto> value) {
        this.resources = value;
    }
}
