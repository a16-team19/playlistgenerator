package com.teamwork.playlistgenerator.models.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceDto {

    private String id;
    private Integer travelDuration;

    @JsonProperty("id")
    public String getID() { return id; }
    @JsonProperty("id")
    public void setID(String value) { this.id = value; }

    @JsonProperty("travelDuration")
    public Integer getTravelDuration() { return travelDuration; }

    @JsonProperty("travelDuration")
    public void setTravelDuration(Integer value) { this.travelDuration = value; }


}
