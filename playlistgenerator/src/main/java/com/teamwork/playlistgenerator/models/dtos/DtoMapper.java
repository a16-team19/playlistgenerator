package com.teamwork.playlistgenerator.models.dtos;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.teamwork.playlistgenerator.models.Album;
import com.teamwork.playlistgenerator.models.Playlist;
import com.teamwork.playlistgenerator.models.User;
import com.teamwork.playlistgenerator.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.Principal;

@Component
public class DtoMapper {
    private final UserService userService;

    @Autowired
    public DtoMapper(UserService userService) {
        this.userService = userService;
    }

    public UserInfoDto fromPrincipalToUserInfo(Principal principal) {
        UserInfoDto userInfoDto = new UserInfoDto();
        if (principal != null) {
            User user = userService.getByUsername(principal.getName());
            userInfoDto.setUsername(user.getUsername());
            userInfoDto.setFirstName(user.getUserDetails().getFirstName());
            userInfoDto.setLastName(user.getUserDetails().getLastName());
            userInfoDto.setEmail(user.getUserDetails().getEmail());
            userInfoDto.setGender(user.getUserDetails().getGender());
            userInfoDto.setPictureURL(user.getUserDetails().getPictureURL());
        }
        return userInfoDto;
    }

    public PlaylistInfoDto fromPlayListToInfoDto(Playlist playlist) {
        PlaylistInfoDto playlistInfoDto = new PlaylistInfoDto();
        playlistInfoDto.setId(playlist.getId());
        playlistInfoDto.setTitle(playlist.getTitle());
        playlistInfoDto.setDuration(playlist.getDuration());
        playlistInfoDto.setUsername(userService.getByUserId(playlist.getUserId()).getUsername());
        playlistInfoDto.setTracklist(playlist.getTracklist());
        playlistInfoDto.setGenreList(playlist.getGenreList());
        playlistInfoDto.setPicUrl(playlistInfoDto.getGenreList().get(0).getPicture());

        return playlistInfoDto;
    }
}
