package com.teamwork.playlistgenerator.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user_details")
public class UserDetails {

    private static final String FIRST_NAME_IS_TOO_LONG = "First name is too long!";
    private static final String LAST_NAME_IS_TOO_LONG = "Last name is too long!";
    private static final String AGE_RANGE = "Your age must be between 1-150!";

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_details_id")
    private int id;

    @Column(name = "email")
    private String email;

    @Size(max = 50, message = FIRST_NAME_IS_TOO_LONG)
    @Column(name = "first_name")
    private String firstName;

    @Size(max = 50, message = LAST_NAME_IS_TOO_LONG)
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "gender")
    private String gender;

    @Column(name = "picture_URL")
    private String pictureURL;
}
