package com.teamwork.playlistgenerator.models.dtos;

import com.teamwork.playlistgenerator.models.Genre;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GeneratePlaylistDto {

    public static final String NAME_EXCEPTION = "Name has to be between 2 and 15 characters!";
    public static final String DIRECTION_EMPTY_EXCEPTION = "Direction cannot be empty!";
    public static final String GENRES_EMPTY_EXCEPTION = "There must be at least one genre!";

    @Size(min = 1,message = NAME_EXCEPTION)
    private String name;

    @Size(min = 1,message = DIRECTION_EMPTY_EXCEPTION)
    private String from;

    @Size(min = 1,message = DIRECTION_EMPTY_EXCEPTION)
    private String to;

    private String username;

    @NotEmpty(message = GENRES_EMPTY_EXCEPTION)
    private List<Genre> genres;
}
