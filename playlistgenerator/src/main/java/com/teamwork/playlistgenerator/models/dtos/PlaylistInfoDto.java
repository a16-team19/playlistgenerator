package com.teamwork.playlistgenerator.models.dtos;

import com.teamwork.playlistgenerator.models.Genre;
import com.teamwork.playlistgenerator.models.Track;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class PlaylistInfoDto {

    private Integer id;

    private String title;

    private Integer duration;

    private String username;

    private String picUrl;

    private List<Track> tracklist;

    private List<Genre> genreList;
}
