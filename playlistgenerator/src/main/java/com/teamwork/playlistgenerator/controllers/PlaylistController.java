package com.teamwork.playlistgenerator.controllers;

import com.teamwork.playlistgenerator.models.Playlist;
import com.teamwork.playlistgenerator.models.dtos.DtoMapper;
import com.teamwork.playlistgenerator.models.dtos.GeneratePlaylistDto;
import com.teamwork.playlistgenerator.services.contracts.GenreService;
import com.teamwork.playlistgenerator.services.contracts.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.concurrent.ExecutionException;

@Controller
public class PlaylistController {

    private final PlaylistService playlistService;
    private final GenreService genreService;
    private final DtoMapper dtoMapper;

    @Autowired
    public PlaylistController(PlaylistService playlistService, GenreService genreService, DtoMapper dtoMapper) {
        this.playlistService = playlistService;
        this.genreService = genreService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/playlist/{id}")
    public ModelAndView getPlaylist(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("playlist");
        mav.addObject("playlist", dtoMapper.fromPlayListToInfoDto(playlistService.getPlaylistById(id)));
        mav.addObject("firstTrack", playlistService.getPlaylistById(id).getTracklist().get(0));
        return mav;
    }

    @GetMapping("/playlists")
    public ModelAndView getAllPlaylists(Principal principal) {
        ModelAndView mav = new ModelAndView("playlists");
        mav.addObject("playlists", playlistService.getAllPlaylistsDto());
        mav.addObject("currentUser", dtoMapper.fromPrincipalToUserInfo(principal));
        return mav;
    }

    @PostMapping("/playlist/generate")
    public String generatePlaylist(@Valid @ModelAttribute GeneratePlaylistDto generatePlaylistDto, BindingResult errors,
                                   Principal principal, Model model) {

        model.addAttribute("currentUser", dtoMapper.fromPrincipalToUserInfo(principal));
        model.addAttribute("playlist", new GeneratePlaylistDto());
        model.addAttribute("genres", genreService.getGenresLoadedTracks());
        model.addAttribute("playlists", playlistService.getRandomFourPlayLists());

        if (errors.hasErrors()) {
            model.addAttribute("error", "Please fill all fields!");
            return "index";
        }
        Playlist playlist;
        try {
            if (playlistService.checkPlaylistExist(generatePlaylistDto.getName()).get()) {
                model.addAttribute("error", "Playlist with the same name already exist!");
                return "index";
            }
            generatePlaylistDto.setUsername(principal.getName());
            playlist = playlistService.generatePlaylist(generatePlaylistDto).get();
        } catch
        (ExecutionException | InterruptedException ex) {
            ex.printStackTrace();
            model.addAttribute("error", "Pick correct locations!");
            return "index";
        }

        return "redirect:/playlist/" + playlist.getId();
    }


}
