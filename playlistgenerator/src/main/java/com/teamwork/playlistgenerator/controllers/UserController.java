package com.teamwork.playlistgenerator.controllers;

import com.teamwork.playlistgenerator.models.dtos.UserDto;
import com.teamwork.playlistgenerator.services.contracts.PlaylistService;
import com.teamwork.playlistgenerator.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class UserController {

    private final UserService userService;
    private final PlaylistService playlistService;


    @Autowired
    public UserController(UserService userService, PlaylistService playlistService) {
        this.userService = userService;
        this.playlistService = playlistService;
    }

    @GetMapping("/profile")
    public ModelAndView showProfile(Principal principal) {
        ModelAndView mav = new ModelAndView("profile");
        mav.addObject("user", userService.getByUsername(principal.getName()));
        mav.addObject("playlists",userService.getAllPlaylistsForUser(principal.getName()));
        mav.addObject("updateUser",new UserDto());
        return mav;
    }

    @PostMapping("/profile/edit")
    public ModelAndView editUser(Principal principal, @Valid @ModelAttribute UserDto updatedUserInfo) {
        ModelAndView mav = new ModelAndView("redirect:/profile");
        updatedUserInfo.setUsername(principal.getName());
        userService.updateUser(updatedUserInfo);
        return mav;
    }

    @PostMapping("/profile/playlist/{id}")
    public ModelAndView deletePlaylist(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("redirect:/profile");
        playlistService.deletePlayListById(id);
        return mav;
    }
}
