package com.teamwork.playlistgenerator.controllers;

import com.teamwork.playlistgenerator.models.dtos.UserRegisterDto;
import com.teamwork.playlistgenerator.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    public static final String DUPLICATE_USERNAME_ERROR_MESSAGE = "User with the same username already exists!";
    public static final String PASSWORDS_DO_NOT_MATCH_ERROR_MESSAGE = "Passwords do not match!";
    private final UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/register")
    public ModelAndView showRegisterPage() {
        ModelAndView mav = new ModelAndView("registration");
        mav.addObject("user", new UserRegisterDto());
        return mav;
    }

    @PostMapping("/register")
    public String registerUser(@Valid UserRegisterDto user, BindingResult errors, Model model) {

        if (errors.hasErrors()) {
            model.addAttribute("user", new UserRegisterDto());
            return "registration";
        }

        if (userService.userExist(user.getUsername())) {
            model.addAttribute("error", DUPLICATE_USERNAME_ERROR_MESSAGE);
            model.addAttribute("user", new UserRegisterDto());
            return "registration";
        }

        if (!user.getPassword().equals(user.getPasswordConfirm())) {
            model.addAttribute("error", PASSWORDS_DO_NOT_MATCH_ERROR_MESSAGE);
            model.addAttribute("user", new UserRegisterDto());
            return "registration";
        }

        userService.createUser(user);
        userService.addDefaultUserDetails(user.getUsername());

        return "/login";
    }

}
