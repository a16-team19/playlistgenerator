package com.teamwork.playlistgenerator.controllers;

import com.teamwork.playlistgenerator.models.dtos.DtoMapper;
import com.teamwork.playlistgenerator.models.dtos.TrackLoadDto;
import com.teamwork.playlistgenerator.models.dtos.UserDto;
import com.teamwork.playlistgenerator.services.contracts.GenreService;
import com.teamwork.playlistgenerator.services.contracts.PlaylistService;
import com.teamwork.playlistgenerator.services.contracts.TrackService;
import com.teamwork.playlistgenerator.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class AdminController {
    private final UserService userService;
    private final TrackService trackService;
    private final GenreService genreService;
    private final PlaylistService playlistService;
    private final DtoMapper mapper;

    @Autowired
    public AdminController(UserService userService, TrackService trackService, GenreService genreService, PlaylistService playlistService, DtoMapper mapper) {
        this.userService = userService;
        this.trackService = trackService;
        this.genreService = genreService;
        this.playlistService = playlistService;
        this.mapper = mapper;
    }

    @GetMapping("/admin/users")
    public ModelAndView showAllUsers(Principal principal) {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", userService.getAll());
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        mav.addObject("updateUser",new UserDto());
        return mav;
    }

    @GetMapping("/admin/tracks")
    public ModelAndView showAllTracks(Principal principal) {
        ModelAndView mav = new ModelAndView("tracks");
        mav.addObject("tracks", trackService.getAll());
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        return mav;
    }

    @PostMapping("/admin/tracks/load")
    public ModelAndView loadTracks(Principal principal, TrackLoadDto trackLoadDto) {
        ModelAndView mav = new ModelAndView("redirect:/admin/tracks");
        trackService.loadTracks(trackLoadDto.getGenre());
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        return mav;
    }

    @GetMapping("/admin/tracks/load")
    public ModelAndView loadTracksView (Principal principal) {
        ModelAndView mav = new ModelAndView("loadtracks");
        mav.addObject("load",new TrackLoadDto());
        mav.addObject("genres",genreService.getGenre());
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        return mav;
    }

    @GetMapping("/admin/genres/load")
    public ModelAndView loadGenre(Principal principal) {
        ModelAndView mav = new ModelAndView("redirect:/admin/genres");
        genreService.loadGenre();
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        return mav;
    }

    @GetMapping("/admin/genres")
    public ModelAndView showAllGenre(Principal principal) {
        ModelAndView mav = new ModelAndView("genres");
        mav.addObject("genres",genreService.getGenre());
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        return mav;
    }

    @GetMapping("/admin/playlists")
    public ModelAndView showAllPlaylists(Principal principal) {
        ModelAndView mav = new ModelAndView("adminplaylists");
        mav.addObject("playlists", playlistService.getAllPlaylists());
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        return mav;
    }

    @PostMapping("/admin/playlist/{id}")
    public ModelAndView deletePlaylist(@PathVariable int id) {
        ModelAndView mav = new ModelAndView("redirect:/admin/playlists");
        playlistService.deletePlayListById(id);
        return mav;
    }

    @GetMapping("/admin")
    public ModelAndView admin(Principal principal) {
        ModelAndView mav = new ModelAndView("admin");
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        return mav;

    }

    @PostMapping("/admin/users/edit/{username}")
    public ModelAndView editUser(@PathVariable String username, UserDto updatedUserInfo) {
        ModelAndView mav = new ModelAndView("redirect:/admin/users");
        updatedUserInfo.setUsername(username);
        userService.updateUser(updatedUserInfo);
        mav.addObject("users", userService.getAll());
        return mav;
    }

    @GetMapping("/admin/users/disabled")
    public ModelAndView showAllDisabled(Principal principal) {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", userService.getAllDisabled());
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        return mav;
    }

    @GetMapping("/admin/users/enabled")
    public ModelAndView showAllActive(Principal principal) {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", userService.getAllActive());
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        return mav;
    }

    @PostMapping("/admin/users/disable/{username}")
    public ModelAndView disableUser(@PathVariable String username) {
        ModelAndView mav = new ModelAndView("redirect:/admin/users");
        userService.disableUser(username);
        mav.addObject("users", userService.getAll());
        return mav;
    }

    @PostMapping("/admin/users/enable/{username}")
    public ModelAndView enableUser(@PathVariable String username) {
        ModelAndView mav = new ModelAndView("redirect:/admin/users");
        userService.enableUser(username);
        mav.addObject("users", userService.getAll());
        return mav;
    }
}
