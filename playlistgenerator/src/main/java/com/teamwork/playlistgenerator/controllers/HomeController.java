package com.teamwork.playlistgenerator.controllers;

import com.teamwork.playlistgenerator.models.dtos.DtoMapper;
import com.teamwork.playlistgenerator.models.dtos.GeneratePlaylistDto;
import com.teamwork.playlistgenerator.services.contracts.GenreService;
import com.teamwork.playlistgenerator.services.contracts.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@PropertySource("classpath:application.properties")
@Controller
public class HomeController {

    private DtoMapper mapper;
    private final GenreService genreService;
    private final PlaylistService playlistService;


    @Autowired
    public HomeController(DtoMapper mapper, GenreService genreService, PlaylistService playlistService) {
        this.mapper = mapper;
        this.genreService = genreService;
        this.playlistService = playlistService;
    }


    @GetMapping({"/", "/home"})
    public ModelAndView showHomePage(Principal principal) {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("currentUser", mapper.fromPrincipalToUserInfo(principal));
        mav.addObject("playlist", new GeneratePlaylistDto());
        mav.addObject("genres", genreService.getGenresLoadedTracks());
        mav.addObject("playlists", playlistService.getRandomFourPlayLists());
        return mav;
    }
}
