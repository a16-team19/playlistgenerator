package com.teamwork.playlistgenerator.services.contracts;

import com.teamwork.playlistgenerator.models.Playlist;
import com.teamwork.playlistgenerator.models.User;
import com.teamwork.playlistgenerator.models.dtos.UserDto;
import com.teamwork.playlistgenerator.models.dtos.UserRegisterDto;

import java.util.List;

public interface UserService {

    void addDefaultUserDetails(String username);

    List<Playlist> getAllPlaylistsForUser(String username);

    List<User> getAll();

    List<User> getAllActive();

    List<User> getAllDisabled();

    void disableUser(String username);

    void enableUser(String username);

    void createUser(UserRegisterDto user);

    boolean userExist(String username);

    User getByUsername(String username);

    User getByUserId(Integer userId);

    void updateUser(UserDto user);


}
