package com.teamwork.playlistgenerator.services.contracts;

import org.springframework.web.multipart.MultipartFile;

public interface ImageFileService {
    String uploadFile(MultipartFile file, String name);
}
