package com.teamwork.playlistgenerator.services;

import com.teamwork.playlistgenerator.models.Genre;
import com.teamwork.playlistgenerator.models.dtos.GenreListDto;
import com.teamwork.playlistgenerator.repositories.contracts.GenreRepository;
import com.teamwork.playlistgenerator.repositories.contracts.TrackRepository;
import com.teamwork.playlistgenerator.services.contracts.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    private static final String HTTPS_API_DEEZER_COM_GENRE = "https://api.deezer.com/genre";
    private static final int VALID_GENRE_COUNT = 100;
    private final GenreRepository genreRepository;
    private final TrackRepository trackRepository;

    @Autowired
    public GenreServiceImpl(GenreRepository genreRepository, TrackRepository trackRepository) {
        this.genreRepository = genreRepository;
        this.trackRepository = trackRepository;
    }

    @Override
    public void loadGenre() {
        RestTemplateBuilder builder = new RestTemplateBuilder();
        RestTemplate restTemplate = builder.build();
        GenreListDto genreList = restTemplate.getForObject(HTTPS_API_DEEZER_COM_GENRE, GenreListDto.class);
        List<Genre> genres = genreList.getGenreList();

        for (Genre genre : genres) {
            if (genreRepository.findByName(genre.getName()) == null) {
                genreRepository.save(genre);
            }
        }
    }

    @Override
    public List<Genre> getGenresLoadedTracks() {
        List<Genre> genres = genreRepository.findAll();

        genres.removeIf(genre -> trackRepository.countByGenre(genre) < VALID_GENRE_COUNT);

        return genres;
    }

    @Override
    public List<Genre> getGenre() {
        return genreRepository.findAll();
    }
}
