package com.teamwork.playlistgenerator.services.contracts;

import com.teamwork.playlistgenerator.models.Playlist;
import com.teamwork.playlistgenerator.models.dtos.GeneratePlaylistDto;
import com.teamwork.playlistgenerator.models.dtos.PlaylistInfoDto;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface PlaylistService {

    void deletePlayListById(Integer id);

    CompletableFuture<Boolean> checkPlaylistExist(String name);

    Playlist getPlaylistById(int id);

    List<Playlist> getAllPlaylists();

    CompletableFuture<Playlist> generatePlaylist(GeneratePlaylistDto generatePlaylistDto);

    List<PlaylistInfoDto> getAllPlaylistsDto();

    List<PlaylistInfoDto> getRandomFourPlayLists();
}
