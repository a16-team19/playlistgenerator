package com.teamwork.playlistgenerator.services.contracts;

import com.teamwork.playlistgenerator.models.Track;

import java.util.List;

public interface TrackService {

    void loadTracks(String genreName);

    List<Track> getAll();
}
