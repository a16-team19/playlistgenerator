package com.teamwork.playlistgenerator.services;

import com.teamwork.playlistgenerator.models.Genre;
import com.teamwork.playlistgenerator.models.Playlist;
import com.teamwork.playlistgenerator.models.Track;
import com.teamwork.playlistgenerator.models.dtos.DtoMapper;
import com.teamwork.playlistgenerator.models.dtos.GeneratePlaylistDto;
import com.teamwork.playlistgenerator.models.dtos.PlaylistInfoDto;
import com.teamwork.playlistgenerator.models.dtos.TravelDurationDto;
import com.teamwork.playlistgenerator.repositories.contracts.PlaylistRepository;
import com.teamwork.playlistgenerator.repositories.contracts.TrackRepository;
import com.teamwork.playlistgenerator.repositories.contracts.UserRepository;
import com.teamwork.playlistgenerator.services.contracts.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    private static final String BING_MAPS_TRAVEL_DURATION_API_URL = "http://dev.virtualearth.net/REST/V1/Routes/Driving?" +
            "wp.0=%s" +
            "&wp.1=%s" +
            "&key=AphLrLUWMuwT-NQrnPJra2UmGokYnUrePttpgb38D1bEbtXwk3joOrWxY0VfpUzu";
    private static final String ALL_GENRES = "All";
    private static final String INVALID_LOCATIONS_EXCEPTION_MESSAGE = "Invalid locations";
    private static final int RANDOM_PLAYLISTS_SIZE = 4;

    private final TrackRepository trackRepository;
    private final PlaylistRepository playlistRepository;
    private final UserRepository userRepository;
    private final DtoMapper dtoMapper;

    @Autowired
    public PlaylistServiceImpl(TrackRepository trackRepository, PlaylistRepository playlistRepository, UserRepository userRepository, DtoMapper dtoMapper) {
        this.trackRepository = trackRepository;
        this.playlistRepository = playlistRepository;
        this.userRepository = userRepository;
        this.dtoMapper = dtoMapper;
    }

    @Override
    @Async
    public CompletableFuture<Playlist> generatePlaylist(GeneratePlaylistDto generatePlaylistDto) {

        Playlist playlist = new Playlist();
        playlist.setTitle(generatePlaylistDto.getName());
        playlist.setGenreList(generatePlaylistDto.getGenres());
        int timeDuration = 0;
        try {
            timeDuration = getTravelTime(generatePlaylistDto.getFrom(), generatePlaylistDto.getTo());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        if (timeDuration == 0) {
            throw new IllegalArgumentException(INVALID_LOCATIONS_EXCEPTION_MESSAGE);
        }
        int timeForGenre = timeDuration / generatePlaylistDto.getGenres().size();

        playlist.setDuration(timeDuration);

        playlist.setTracklist(getPlaylistTracks(timeForGenre, generatePlaylistDto.getGenres()));

        playlist.setUserId(userRepository.findByUsername(generatePlaylistDto.getUsername()).getId());

        playlistRepository.save(playlist);

        return CompletableFuture.completedFuture(playlist);
    }

    @Override
    public Playlist getPlaylistById(int id) {
        return playlistRepository.getOne(id);
    }

    @Override
    public void deletePlayListById(Integer id) {
        playlistRepository.deleteById(id);
    }

    @Override
    public List<Playlist> getAllPlaylists() {
        return playlistRepository.findAll();
    }

    @Override
    public List<PlaylistInfoDto> getAllPlaylistsDto() {
        List<Playlist> playlists = playlistRepository.findAll();
        List<PlaylistInfoDto> playlistInfoDtos = new ArrayList<>();
        for (Playlist playlist : playlists) {
            playlistInfoDtos.add(dtoMapper.fromPlayListToInfoDto(playlist));
        }
        return playlistInfoDtos;
    }

    @Override
    public List<PlaylistInfoDto> getRandomFourPlayLists() {
        List<Playlist> playlists = playlistRepository.findAll();
        Collections.shuffle(playlists);
        int availablePlaylists = 4;
        if (playlists.size() < RANDOM_PLAYLISTS_SIZE) {
            availablePlaylists = playlists.size();
        }
        List<PlaylistInfoDto> playlistsToReturn = new ArrayList<>();
        for (int i = 0; i < availablePlaylists; i++) {
            playlistsToReturn.add(dtoMapper.fromPlayListToInfoDto(playlists.get(i)));
        }

        return playlistsToReturn;
    }

    @Override
    @Async
    public CompletableFuture<Boolean> checkPlaylistExist(String name) {
        if (playlistRepository.getByTitle(name) != null) {
            return CompletableFuture.completedFuture(true);
        }
        return CompletableFuture.completedFuture(false);
    }

    private int getTravelTime(String from, String to) {

        RestTemplateBuilder builder = new RestTemplateBuilder();
        RestTemplate restTemplate = builder.build();

        TravelDurationDto travelDurationDto = restTemplate.
                getForObject(String.format(BING_MAPS_TRAVEL_DURATION_API_URL, from, to),
                        TravelDurationDto.class);

        return travelDurationDto.getResourceSets().get(0).getResources().get(0).getTravelDuration();
    }


    private List<Track> getPlaylistTracks(int timeForGenre, List<Genre> genres) {
        List<Track> trackList = new ArrayList<>();

        for (Genre genre : genres) {
            List<Track> tracksForGenre = trackRepository.getTracksByGenre(genre);
            Collections.shuffle(tracksForGenre);
            int curDuration = 0;
            for (Track track : tracksForGenre) {
                if (curDuration >= timeForGenre) {
                    break;
                }
                if (!trackList.contains(track)) {
                    trackList.add(track);
                    curDuration += track.getDuration();
                }
            }
        }
        Collections.shuffle(trackList);
        return trackList;
    }

}

