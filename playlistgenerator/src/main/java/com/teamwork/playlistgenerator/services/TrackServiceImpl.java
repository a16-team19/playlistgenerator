package com.teamwork.playlistgenerator.services;

import com.teamwork.playlistgenerator.models.Album;
import com.teamwork.playlistgenerator.models.Artist;
import com.teamwork.playlistgenerator.models.Genre;
import com.teamwork.playlistgenerator.models.Track;
import com.teamwork.playlistgenerator.models.dtos.PlayListDto;
import com.teamwork.playlistgenerator.models.dtos.TrackListDto;
import com.teamwork.playlistgenerator.models.dtos.TrackListLinkDto;
import com.teamwork.playlistgenerator.repositories.contracts.AlbumRepository;
import com.teamwork.playlistgenerator.repositories.contracts.ArtistRepository;
import com.teamwork.playlistgenerator.repositories.contracts.GenreRepository;
import com.teamwork.playlistgenerator.repositories.contracts.TrackRepository;
import com.teamwork.playlistgenerator.services.contracts.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

    private static final String HTTPS_API_DEEZER_COM_SEARCH_PLAYLIST = "https://api.deezer.com/search/playlist?q=";
    private static final int DEFAULT_COUNT = 1000;

    private final TrackRepository trackRepository;
    private final ArtistRepository artistRepository;
    private final AlbumRepository albumRepository;
    private final GenreRepository genreRepository;

    @Autowired
    public TrackServiceImpl(TrackRepository trackRepository, ArtistRepository artistRepository, AlbumRepository albumRepository, GenreRepository genreRepository) {
        this.trackRepository = trackRepository;
        this.artistRepository = artistRepository;
        this.albumRepository = albumRepository;
        this.genreRepository = genreRepository;
    }

    @Override
    public void loadTracks(String genreName) {

        RestTemplateBuilder builder = new RestTemplateBuilder();
        RestTemplate restTemplate = builder.build();

        PlayListDto playListDto = getPlayListDto(genreName);
        List<TrackListLinkDto> trackListLinkDtos = playListDto.getTracksList();
        long newCount = trackRepository.count() + DEFAULT_COUNT;

        for (TrackListLinkDto trackListLinkDto : trackListLinkDtos) {
            TrackListDto trackListDto = restTemplate.getForObject(trackListLinkDto.getTracklist(), TrackListDto.class);
            List<Track> tracks = trackListDto.getTracks();
            if (tracks != null) {
                saveTracks(genreName, tracks);
            }
            while (trackListDto.getNext() != null) {
                if (trackRepository.count() < newCount) {
                    break;
                }
                trackListDto = restTemplate.getForObject(trackListDto.getNext(), TrackListDto.class);
                tracks = trackListDto.getTracks();
                saveTracks(genreName, tracks);
            }
        }
    }


    @Override
    public List<Track> getAll() {
        return trackRepository.findAll();
    }

    private PlayListDto getPlayListDto(String genre) {
        RestTemplateBuilder builder = new RestTemplateBuilder();
        RestTemplate restTemplate = builder.build();

        String url = HTTPS_API_DEEZER_COM_SEARCH_PLAYLIST + genre;

        return restTemplate.getForObject(url, PlayListDto.class);

    }

    private void saveTracks(String genreName, List<Track> tracks) {
        for (Track track : tracks) {
            Track newTrack = trackRepository.findByTitle(track.getTitle());
            if (newTrack == null) {
                if ((track.getPreview() != null && !track.getPreview().equals("")) && track.getAlbum() != null && track.getArtist() != null) {
                    Album album = albumRepository.findByTitle(track.getAlbum().getTitle());
                    if (album == null) {
                        Album newAlbum = track.getAlbum();
                        albumRepository.save(newAlbum);
                        track.setAlbum(albumRepository.findByTitle(newAlbum.getTitle()));
                    } else {
                        track.setAlbum(album);
                    }

                    Artist artist = artistRepository.findByName(track.getArtist().getName());
                    if (artist == null) {
                        Artist newArtist = track.getArtist();
                        artistRepository.save(newArtist);
                        track.setArtist(artistRepository.findByName(newArtist.getName()));
                    } else {
                        track.setArtist(artist);
                    }

                    Genre genre = genreRepository.findByName(genreName);
                    if (genre == null) {
                        Genre newGenre = new Genre();
                        newGenre.setName(genreName);
                        genreRepository.save(newGenre);
                        track.setGenre(genreRepository.findByName(genreName));
                    } else {
                        track.setGenre(genre);
                    }
                    trackRepository.save(track);
                }
            }
        }
    }

}
