package com.teamwork.playlistgenerator.services;

import com.teamwork.playlistgenerator.exceptions.FileStorageException;
import com.teamwork.playlistgenerator.services.contracts.ImageFileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

@Service
public class ImageFileServiceImpl implements ImageFileService {

    private static final String FILE_STORAGE_EX = "Could not store file %s. Please try again!";
    private static final String FILE_CANNOT_BE_NULL = "File cannot be null!";

    @Value("src/main/webapp/WEB-INF/images/")
    public String uploadDir;

    @Override
    public String uploadFile(MultipartFile file, String name) {
        if (file == null){
            throw  new NullPointerException(FILE_CANNOT_BE_NULL);
        }
        String[] contents = Objects.requireNonNull(file.getContentType()).split("/");
        String content = contents[1];
        try {
            Path copyLocation = Paths
                    .get(uploadDir + File.separator + StringUtils.cleanPath(name + "." + content));
            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileStorageException(String.format(FILE_STORAGE_EX,file.getOriginalFilename()));
        }

        return "/images/" + name + "." + content;
    }
}

