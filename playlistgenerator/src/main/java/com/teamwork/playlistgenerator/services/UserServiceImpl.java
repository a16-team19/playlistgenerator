package com.teamwork.playlistgenerator.services;

import com.teamwork.playlistgenerator.models.Playlist;
import com.teamwork.playlistgenerator.models.User;
import com.teamwork.playlistgenerator.models.UserDetails;
import com.teamwork.playlistgenerator.models.dtos.UserDto;
import com.teamwork.playlistgenerator.models.dtos.UserRegisterDto;
import com.teamwork.playlistgenerator.repositories.contracts.PlaylistRepository;
import com.teamwork.playlistgenerator.repositories.contracts.UserRepository;
import com.teamwork.playlistgenerator.services.contracts.ImageFileService;
import com.teamwork.playlistgenerator.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final int ENABLED = 1;
    private static final int DISABLED = 0;
    public static final String IMAGE_PATH_FOR_USER = "users/";
    private static final String DEFAULT_USER_IMAGE = "/images/users/default-avatar.png";
    private static final String USER_DONT_EXIST = "This user does not exist!";
    private final UserRepository userRepository;
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;
    private final ImageFileService imageFileService;
    private final PlaylistRepository playlistRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, ImageFileService imageFileService, PlaylistRepository playlistRepository) {
        this.userRepository = userRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.imageFileService = imageFileService;
        this.playlistRepository = playlistRepository;
    }

    @Override
    public void createUser(UserRegisterDto user) {

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");

        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        passwordEncoder.encode(user.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);
    }

    @Override
    public boolean userExist(String username) {
        return userDetailsManager.userExists(username);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User getByUserId(Integer userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()){
            return user.get();
        } else {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public void updateUser(UserDto newUser) {
        User userToUpdate = getByUsername(newUser.getUsername());
        userToUpdate.getUserDetails().setFirstName(newUser.getFirstName());
        userToUpdate.getUserDetails().setLastName(newUser.getLastName());
        userToUpdate.getUserDetails().setEmail(newUser.getEmail());
        userToUpdate.getUserDetails().setGender(newUser.getGender());
        if (!newUser.getFile().isEmpty()) {
            userToUpdate.getUserDetails().setPictureURL(imageFileService.uploadFile(newUser.getFile(),
                    IMAGE_PATH_FOR_USER + newUser.getUsername()));
        }
        userRepository.save(userToUpdate);
    }

    @Override
    public void addDefaultUserDetails(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException(USER_DONT_EXIST);
        }
        UserDetails userDetails = new UserDetails();
        userDetails.setPictureURL(DEFAULT_USER_IMAGE);
        user.setUserDetails(userDetails);

        userRepository.save(user);
    }

    @Override
    public List<Playlist> getAllPlaylistsForUser(String username){
        User user = getByUsername(username);
        return playlistRepository.findAllByUserId(user.getId());
    }
    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getAllActive() {
        return userRepository.findAllByEnabledEquals(ENABLED);
    }

    @Override
    public List<User> getAllDisabled() {
        return userRepository.findAllByEnabledEquals(DISABLED);
    }

    @Override
    public void disableUser(String username){
        userRepository.disableUser(getByUsername(username).getId());
    }

    @Override
    public void enableUser(String username){
        userRepository.enableUser(getByUsername(username).getId());
    }
}
