package com.teamwork.playlistgenerator.services.contracts;

import com.teamwork.playlistgenerator.models.Genre;

import java.util.List;

public interface GenreService {
    List<Genre> getGenresLoadedTracks();
    void loadGenre();

    List<Genre> getGenre();
}
