package com.teamwork.playlistgenerator.repositories.contracts;

import com.teamwork.playlistgenerator.models.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlaylistRepository extends JpaRepository<Playlist, Integer> {

    List<Playlist> findAllByUserId(Integer userId);

    Playlist getByTitle(String string);
}
