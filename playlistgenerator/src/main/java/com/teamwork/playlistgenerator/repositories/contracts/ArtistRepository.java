package com.teamwork.playlistgenerator.repositories.contracts;

import com.teamwork.playlistgenerator.models.Album;
import com.teamwork.playlistgenerator.models.Artist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistRepository extends JpaRepository<Artist,Integer> {

     Artist findByName(String name);
}
