package com.teamwork.playlistgenerator.repositories.contracts;

import com.teamwork.playlistgenerator.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface UserRepository extends JpaRepository<User,Integer> {

    User findByUsername(String username);

    List<User> findAllByEnabledEquals(Integer enabled);

    @Transactional
    @Modifying
    @Query("update User u set u.enabled = 0 WHERE u.id = :userId")
    void disableUser( @Param("userId") Integer userId);

    @Transactional
    @Modifying
    @Query("update User u set u.enabled = 1 WHERE u.id = :userId")
    void enableUser(@Param("userId") Integer userId);


}
