package com.teamwork.playlistgenerator.repositories.contracts;

import com.teamwork.playlistgenerator.models.Album;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlbumRepository extends JpaRepository<Album,Integer> {

     Album findByTitle(String title);
}
