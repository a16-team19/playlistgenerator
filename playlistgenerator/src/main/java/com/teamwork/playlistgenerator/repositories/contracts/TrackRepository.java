package com.teamwork.playlistgenerator.repositories.contracts;

import com.teamwork.playlistgenerator.models.Genre;
import com.teamwork.playlistgenerator.models.Track;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TrackRepository extends JpaRepository<Track,Integer> {

    Track findByTitle(String title);

    List<Track> getTracksByGenre(Genre genre);

    Integer countByGenre(Genre genre);
}
