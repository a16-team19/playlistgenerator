package com.teamwork.playlistgenerator.repositories.contracts;

import com.teamwork.playlistgenerator.models.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository  extends JpaRepository<Genre,Integer> {

     Genre findByName(String name);
}
