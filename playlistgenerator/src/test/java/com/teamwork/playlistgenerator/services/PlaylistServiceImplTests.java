package com.teamwork.playlistgenerator.services;

import com.teamwork.playlistgenerator.models.Playlist;
import com.teamwork.playlistgenerator.models.dtos.DtoMapper;
import com.teamwork.playlistgenerator.models.dtos.GeneratePlaylistDto;
import com.teamwork.playlistgenerator.models.dtos.PlaylistInfoDto;
import com.teamwork.playlistgenerator.repositories.contracts.PlaylistRepository;
import com.teamwork.playlistgenerator.repositories.contracts.TrackRepository;
import com.teamwork.playlistgenerator.repositories.contracts.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.teamwork.playlistgenerator.Factory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PlaylistServiceImplTests {


    @Mock
    PlaylistRepository playlistRepository;

    @Mock
    TrackRepository trackRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    DtoMapper dtoMapper;

    @InjectMocks
    PlaylistServiceImpl playlistService;


    @Test
    public void getShould_ReturnAllPlaylists_WhenExists() {
        //Arrange

        Mockito.when(playlistRepository.findAll()).thenReturn(Arrays.asList(
                createPlaylist(), createPlaylist()
        ));

        // Act
        List<Playlist> result = playlistService.getAllPlaylists();

        // Assert
        Assert.assertSame(PLAYLIST_TITLE, (result.get(0)).getTitle());
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getByIdShould_ReturnPlaylist() {
        //Arrange

        Mockito.when(playlistRepository.getOne(anyInt())).thenReturn(createPlaylist()
        );

        // Act
        Playlist result = playlistService.getPlaylistById(anyInt());

        // Assert
        Assert.assertSame(PLAYLIST_TITLE, result.getTitle());
    }

    @Test
    public void deletePlaylistShould_deletePlaylist() {
        // Arrange
        Playlist playlist = createPlaylist();

        // Act
        playlistService.deletePlayListById(playlist.getId());

        // Assert
        verify(playlistRepository, Mockito.times(1))
                .deleteById(anyInt());
    }

    @Test
    public void getAllPlaylistsDtoShould_ReturnPlaylistDto() {
        //Arrange

        Mockito.when(playlistRepository.findAll()).thenReturn(Arrays.asList(
                createPlaylist(), createPlaylist()
        ));

        // Act
        List<PlaylistInfoDto> result = playlistService.getAllPlaylistsDto();

        // Assert
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getRandomFourPlayListsDtoShould_ReturnPlaylistDto() {
        //Arrange
        Mockito.when(playlistRepository.findAll()).thenReturn(Arrays.asList(
                createPlaylist(), createPlaylist()
        ));

        // Act
        List<PlaylistInfoDto> result = playlistService.getRandomFourPlayLists();

        // Assert
        Assert.assertEquals(2, result.size());
    }


    @Test
    public void generatePlaylistShould_ReturnPlaylist() throws ExecutionException, InterruptedException {
        //Arrange
        Mockito.when(trackRepository.getTracksByGenre(any()))
                .thenReturn(createTrackList());

        Mockito.when(userRepository.findByUsername(anyString()))
                .thenReturn(createUser());

        // Act
        Playlist result = playlistService.generatePlaylist(createGeneratePlaylistDto()).get();

        // Assert
        Assert.assertEquals(PLAYLIST_NAME, result.getTitle());

    }

    @Test(expected = IllegalArgumentException.class)
    public void generatePlaylistShould_ThrowExceptionWhen_InvalidLocation() throws ExecutionException, InterruptedException {

        //Arrange
        GeneratePlaylistDto generatePlaylistDto = createGeneratePlaylistDto();
        generatePlaylistDto.setFrom(INVALID_LOCATION);

        // Act Assert
        Mockito.when(playlistService.generatePlaylist(generatePlaylistDto)).thenThrow(new IllegalArgumentException(INVALID_LOCATION));
    }

    @Test
    public void checkPlaylistExistShould_ReturnTrueWhen_PlayerExist() throws ExecutionException, InterruptedException {

        //Arrange
        Playlist playlist = createPlaylist();
        Mockito.when(playlistRepository.getByTitle(anyString())).thenReturn(playlist);

        // Act Assert
        Assert.assertEquals(playlistService.checkPlaylistExist(playlist.getTitle()).get(), true);
    }

    @Test
    public void checkPlaylistExistShould_ReturnFalseWhen_PlayerNotExist() throws ExecutionException, InterruptedException {

        //Arrange
        Playlist playlist = createPlaylist();
        Mockito.when(playlistRepository.getByTitle(anyString())).thenReturn(null);

        // Act Assert
        Assert.assertEquals(playlistService.checkPlaylistExist(playlist.getTitle()).get(), false);
    }
}
