package com.teamwork.playlistgenerator.services;

import com.teamwork.playlistgenerator.models.Genre;
import com.teamwork.playlistgenerator.repositories.contracts.GenreRepository;
import com.teamwork.playlistgenerator.repositories.contracts.TrackRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.teamwork.playlistgenerator.Factory.createGenre;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class GenreServiceImplTests {

    @Mock
    GenreRepository genreRepository;

    @Mock
    TrackRepository trackRepository;

    @InjectMocks
    GenreServiceImpl genreService;

    @Test
    public void getGenresLoadedGenreShould_ReturnAllGenreAbove_100_Tracks() {
        //Arrange

        Mockito.when(genreRepository.findAll()).thenReturn(Arrays.asList(
                createGenre(),createGenre()
        ));

        Mockito.when(trackRepository.countByGenre(anyObject())).thenReturn(101);

        // Act
        List<Genre> result = genreService.getGenresLoadedTracks();

        // Assert
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getShould_GetAllGenre() {
        //Arrange
        Mockito.when(genreRepository.findAll()).thenReturn(Arrays.asList(
                createGenre(),createGenre()
        ));

        // Act
        List<Genre> result = genreService.getGenre();

        // Assert
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void loadGenreShould_LoadGenres(){

        // Arrange
        Mockito.when(genreRepository.findByName(anyString())).thenReturn(null);

        // Act
        genreService.loadGenre();

        // Assert
        Mockito.verify(genreRepository, Mockito.atLeast(1)).save(any(Genre.class));
    }

}
