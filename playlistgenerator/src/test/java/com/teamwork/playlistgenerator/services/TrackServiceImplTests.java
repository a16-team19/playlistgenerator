package com.teamwork.playlistgenerator.services;

import com.teamwork.playlistgenerator.models.Track;
import com.teamwork.playlistgenerator.repositories.contracts.AlbumRepository;
import com.teamwork.playlistgenerator.repositories.contracts.ArtistRepository;
import com.teamwork.playlistgenerator.repositories.contracts.GenreRepository;
import com.teamwork.playlistgenerator.repositories.contracts.TrackRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import static com.teamwork.playlistgenerator.Factory.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class TrackServiceImplTests {

    @Mock
    TrackRepository trackRepository;

    @Mock
    AlbumRepository albumRepository;

    @Mock
    ArtistRepository artistRepository;

    @Mock
    GenreRepository genreRepository;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    TrackServiceImpl trackService;


    @Test
    public void getShould_ReturnAllTracks_WhenExists() {
        //Arrange

        Mockito.when(trackRepository.findAll()).thenReturn(Arrays.asList(
                createTrack(), createTrack()
        ));

        // Act
        List<Track> result = trackService.getAll();

        // Assert
        Assert.assertSame(TRACK_TITLE, (result.get(0)).getTitle());
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void loadShould_loadTracksByGenre() {
        // Arrange Act
        trackService.loadTracks(GENRE_NAME);

        // Assert
        Mockito.verify(trackRepository, Mockito.atLeast(500)).save(any(Track.class));

    }
}
