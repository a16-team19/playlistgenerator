package com.teamwork.playlistgenerator.services;

import com.teamwork.playlistgenerator.models.User;
import com.teamwork.playlistgenerator.models.dtos.UserRegisterDto;
import com.teamwork.playlistgenerator.repositories.contracts.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.teamwork.playlistgenerator.Factory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @Mock
    UserDetailsManager userDetailsManager;

    @Mock
    PasswordEncoder passwordEncoder;

    @InjectMocks
    UserServiceImpl userService;


    @Test
    public void getShould_ReturnAllUsers_WhenExists() {
        //Arrange

        Mockito.when(userRepository.findAll()).thenReturn(Arrays.asList(
                createUser(),
                createUser()
        ));

        // Act
        List<User> result = userService.getAll();

        // Assert
        Assert.assertSame(USER_NAME, (result.get(0)).getUsername());
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getActiveShould_ReturnAllActiveUsers_WhenExists() {
        //Arrange

        Mockito.when(userRepository.findAllByEnabledEquals(anyInt())).thenReturn(Arrays.asList(
                createUser(),
                createUser()
        ));

        // Act
        List<User> result = userService.getAllActive();

        // Assert
        Assert.assertSame(USER_NAME, (result.get(0)).getUsername());
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getDisabledShould_ReturnAllDisabledUsers_WhenExists() {
        //Arrange

        Mockito.when(userRepository.findAllByEnabledEquals(anyInt())).thenReturn(Arrays.asList(
                createUser(),
                createUser()
        ));

        // Act
        List<User> result = userService.getAllDisabled();

        // Assert
        Assert.assertSame(USER_NAME, (result.get(0)).getUsername());
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getByIdShould_ReturnUser_WhenExist() {
        // Arrange
        User expected = createUser();
        Mockito.when(userRepository.findByUsername(anyString())).thenReturn(createUser());

        // act
        User result = userService.getByUsername(anyString());

        // Assert
        Assert.assertSame(expected.getId(), result.getId());
        Assert.assertSame(expected.getUsername(), result.getUsername());
    }

    @Test
    public void disableUserShould_DisableUser() {
        // Arrange
        User user = createUser();
        Mockito.when(userRepository.findByUsername(anyString())).thenReturn(createUser());

        // Act
        userService.disableUser(user.getUsername());

        // Assert
        verify(userRepository, Mockito.times(1))
                .disableUser(user.getId());
    }

    @Test
    public void enableUserShould_EnableUser() {
        // Arrange
        User user = createUser();
        Mockito.when(userRepository.findByUsername(anyString())).thenReturn(createUser());

        // Act
        userService.enableUser(user.getUsername());

        // Assert
        verify(userRepository, Mockito.times(1))
                .enableUser(user.getId());
    }

    @Test
    public void createUserShould_CreateUser() {
        // Arrange
        UserRegisterDto user = createUserRegisterDto();
        Mockito.when(passwordEncoder.encode(anyString())).thenReturn(anyString());

        // Act
        userService.createUser(user);

        // Assert
        Mockito.verify(userDetailsManager, Mockito.times(1))
                .createUser(new org.springframework.security.core.userdetails.User( user.getUsername(), PASSWORD,
                        AuthorityUtils.createAuthorityList("ROLE_USER")));
    }

    @Test
    public void updateUserShould_UpdateName() {
        // Arrange Act
        Mockito.when(userRepository.findByUsername(anyString())).thenReturn(createUser());
        userService.updateUser(createUserDto());

        // Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .save(any(User.class));
    }

    @Test
    public void addDefaultUserDetailsShould_UpdateUserDetails(){
        // Arrange
        Mockito.when(userRepository.findByUsername(anyString()))
                .thenReturn(createUser());

        // Act
        userService.addDefaultUserDetails(USER_NAME);

        // Assert
        Mockito.verify(userRepository,Mockito.times(1))
                .save(any(User.class));
    }

    @Test(expected= EntityNotFoundException.class)
    public void addDefaultUserDetailsShouldThrowEntityNotFound_UpdateUserDetails(){
        // Arrange
        Mockito.when(userRepository.findByUsername(anyString()))
                .thenReturn(null);

        // Act
        userService.addDefaultUserDetails(USER_NAME);

    }

    @Test(expected= EntityNotFoundException.class)
    public void getByUserIdShould_GetByUserId(){

        // Act
        User result = userService.getByUserId(createUser().getId());

    }
}
