package com.teamwork.playlistgenerator.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.teamwork.playlistgenerator.Factory.FILE_NAME;

@RunWith(MockitoJUnitRunner.class)
public class ImageFileServiceImplTests {

    @InjectMocks
    ImageFileServiceImpl imageFileService;

    @Test(expected = NullPointerException.class)
    public void uploadFileShould_ThrowExceptionWhen_FileIsNull() {

        // Arrange Act Assert
        Mockito.when(imageFileService.uploadFile(null, FILE_NAME)).thenThrow(NullPointerException.class);

    }
}
