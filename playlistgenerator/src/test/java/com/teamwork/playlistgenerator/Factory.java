package com.teamwork.playlistgenerator;

import com.teamwork.playlistgenerator.models.*;
import com.teamwork.playlistgenerator.models.dtos.GeneratePlaylistDto;
import com.teamwork.playlistgenerator.models.dtos.TrackListLinkDto;
import com.teamwork.playlistgenerator.models.dtos.UserDto;
import com.teamwork.playlistgenerator.models.dtos.UserRegisterDto;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Factory {

    public static final String USER_NAME = "userName";
    public static final String DEFAULT_BEER_IMAGE_ADDRESS = "/images/users/default-avatar.jpg";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PASSWORD = "123";
    public static final String GENRE_NAME = "Pop";
    public static final String LOAD_TRACKS = "https://api.deezer.com/search/playlist?q=Pop";
    public static final String TRACK_TITLE = "trackTitle";
    public static final String PLAYLIST_TITLE = "playlistTitle";
    public static final String ALBUM_TITLE = "albumTitle";
    public static final String ARTIST_NAME = "artistName";
    public static final String PLAYLIST_NAME = "Playlist_Name";
    public static final String TEST_LOCATION_FROM = "troyan";
    public static final String TEST_LOCATION_TO = "lovech";
    public static final int USER_ID = 1;
    public static final String INVALID_LOCATION = "Invalid_Location";
    public static final String FILE_NAME = "File_Name";

    public static User createUser() {
        User user = new User();
        user.setId(USER_ID);
        user.setUsername(USER_NAME);
        user.setUserDetails(createUserDetails());
        return user;
    }

    public static UserRegisterDto createUserRegisterDto() {
        UserRegisterDto user = new UserRegisterDto();
        user.setUsername(USER_NAME);
        user.setPassword(PASSWORD);
        user.setPasswordConfirm(PASSWORD);
        return user;
    }

    public static UserDto createUserDto() {
        UserDto user = new UserDto();
        user.setUsername(USER_NAME);
        user.setFile(createFile());
        return user;
    }

    public static UserDetails createUserDetails() {
        UserDetails userDetails = new UserDetails();
        userDetails.setId(1);
        userDetails.setFirstName(FIRST_NAME);
        userDetails.setLastName(LAST_NAME);
        return userDetails;
    }

    public static MultipartFile createFile() {
        return new MockMultipartFile("file", "fileName", "txt", new byte[0]);
    }

    public static Genre createGenre() {
        Genre genre = new Genre();
        genre.setId(1);
        genre.setName(GENRE_NAME);

        return genre;
    }

    public static Album createAlbum() {
        Album album = new Album();
        album.setTitle(ALBUM_TITLE);

        return album;
    }

    public static Artist createArtist() {
        Artist artist = new Artist();
        artist.setName(ARTIST_NAME);

        return artist;
    }

    public static Track createTrack() {
        Track track = new Track();
        track.setId(BigInteger.valueOf(1));
        track.setTitle(TRACK_TITLE);
        track.setArtist(createArtist());
        track.setAlbum(createAlbum());
        track.setGenre(createGenre());
        track.setDuration(Integer.MAX_VALUE);
        return track;
    }

    public static Playlist createPlaylist() {
        Playlist playlist = new Playlist();
        playlist.setId(1);
        playlist.setTitle(PLAYLIST_TITLE);

        return playlist;
    }

    public static TrackListLinkDto createTrackListLinkDto() {
        TrackListLinkDto trackListLinkDto = new TrackListLinkDto();
        trackListLinkDto.setTracklist("");

        return trackListLinkDto;
    }

    public static GeneratePlaylistDto createGeneratePlaylistDto() {
        GeneratePlaylistDto generatePlaylistDto = new GeneratePlaylistDto();

        generatePlaylistDto.setName(PLAYLIST_NAME);
        generatePlaylistDto.setFrom(TEST_LOCATION_FROM);
        generatePlaylistDto.setTo(TEST_LOCATION_TO);

        generatePlaylistDto.setUsername(USER_NAME);

        List<Genre> genres = new ArrayList<>();
        genres.add(createGenre());
        generatePlaylistDto.setGenres(genres);

        return generatePlaylistDto;
    }

    public static List<Track> createTrackList(){
        List<Track> tracks = new ArrayList<>();
        tracks.add(createTrack());

        return tracks;
    }
}
