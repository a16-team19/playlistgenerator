## **Ride Pal**

Ride Pal enables your users to generate playlists for specific travel duration periods based on their preferred genres. The application offers the option to browse playlists created by other users. Users can listen to preview of the tracks. Admins can edit other users and upload tracks and genres.

## 
  - Visible without authentication
    - the application start page
    - the user login form
    - the user registration form
    - browse all playlists

    
  - When authenticated, user can create playlists and listen to playlists created by them and ther users. They can also delete their own playlists. Users can modify their personal information as well.

  - The app have profile page that shows user’s profile picture, first name, last name, email and information about his own playlists
 
  - The admin page shows multiple tables with playlists, users, genres, tracks, supporting the CRUD operations.

### Technologies
- *IntelliJ IDEA* - as a IDE during the whole development process
- *Spring MVC* - as an application framework
- *Bootstrap 4* - for the front-end of the application making it responsive and providing ready to use stylesheets
- *Thymeleaf* - as a template engine in order to load a big amount of data (beers, users). Also thymeleaf allows inserting and replacing views and imports, which makes the code way more readable and replaceable
- *MySQL* - as database
- *JPA* - access the database
- *Spring Security* - for managing users, roles and authentication
- *Git* - for source control management and documentation / manual of the application


### Routes

| Mapping | Description |
| ------ | ------ |
| / | Home page with generate playlist(when authenticated) and random four playlists by users|
| /playlists| Playlist page where all the playlists are shown and they can be played|
| /playlist/{id} | Player to listen to playlists track previews {playlistId} |
| /register | Register page with three must-fill text-boxes|
| /login | Login page providing authentication via username and password|
| /profile | User page providing information about the user |
| /admin/playlists | Admin page where admin can delete playlists|
| /admin/users | Admin page where admin can soft delete or edit users|
| /admin/tracks | Admin page where admin can see all loaded tracks|
| /admin/tracks/load | Admin page where admin can load tracks by genre|
| /admin/genres | Admin page where admin can see all genres|
| /admin/genres/load | Admin page where admin can load all genres from Dizzer|

### Screenshots

- Home without authentication
![home-not-authenticated](/images/home.png)

- Home with authentication/Generate Playlist
![logged-home](/images/loggedingenerate.png)

- Playlists tab
![playliststab](/images/playlistpanel.png)

- Player tab
![player](/images/player.png)

- User profile
![user-profile](/images/profile.png)

- User profile playlist tab
![user-playlists](/images/playliststab.png)

- Edit Profile
![update-profile](/images/edituser.png)

- Login
![login](/images/login.png)

- Register page
![register](/images/register.png)

- Admin Playlist tab
![admin-playlist](/images/playlistadmintab.png)

- Admin Tracks tab
![admin-tracks](/images/tracksadmintab.png)

- Admin Load Tracks tab
![admin-load-tracks](/images/adminloadtracks.png)

- Admin Playlist tab
![admin-playlist](/images/playlistadmintab.png)

- Admin Users tab
![admin-users](/images/useradmintab.png)

- Admin Genre tab
![admin-genre](/images/genreadmintab.png)
