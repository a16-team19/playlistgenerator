-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema ride-pal
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ride-pal
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ride-pal` DEFAULT CHARACTER SET utf8 ;
USE `ride-pal` ;

-- -----------------------------------------------------
-- Table `ride-pal`.`albums`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`albums` (
  `album_id` INT(20) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(250) NOT NULL,
  `tracklist` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`album_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5529
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ride-pal`.`artists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`artists` (
  `artist_id` INT(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(250) NOT NULL,
  `tracklist` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`artist_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3519
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ride-pal`.`user_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`user_details` (
  `user_details_id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(200) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `first_name` VARCHAR(50) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `last_name` VARCHAR(50) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `age` DATETIME NULL DEFAULT NULL,
  `gender` VARCHAR(50) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `picture_URL` VARCHAR(2000) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`user_details_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ride-pal`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(68) NOT NULL,
  `enabled` TINYINT(4) NOT NULL DEFAULT '1',
  `user_details_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `users_username_uindex` (`username` ASC) VISIBLE,
  INDEX `fk_users_user_details1_idx` (`user_details_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_user_details1`
    FOREIGN KEY (`user_details_id`)
    REFERENCES `ride-pal`.`user_details` (`user_details_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ride-pal`.`authorities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`authorities` (
  `username` VARCHAR(50) NOT NULL,
  `authority` VARCHAR(50) NOT NULL,
  INDEX `username` (`username` ASC) VISIBLE,
  CONSTRAINT `authorities_ibfk_1`
    FOREIGN KEY (`username`)
    REFERENCES `ride-pal`.`users` (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ride-pal`.`genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`genres` (
  `genre_id` INT(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `picture` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`genre_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 64
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ride-pal`.`playlists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`playlists` (
  `playlist_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `duration` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`playlist_id`),
  INDEX `fk_playlists_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_playlists_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `ride-pal`.`users` (`user_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ride-pal`.`playlists_genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`playlists_genres` (
  `playlists_playlist_id` INT(11) NOT NULL,
  `genres_genre_id` INT(20) NOT NULL,
  PRIMARY KEY (`playlists_playlist_id`, `genres_genre_id`),
  INDEX `fk_playlists_has_genres_genres1_idx` (`genres_genre_id` ASC) VISIBLE,
  INDEX `fk_playlists_has_genres_playlists1_idx` (`playlists_playlist_id` ASC) VISIBLE,
  CONSTRAINT `fk_playlists_has_genres_genres1`
    FOREIGN KEY (`genres_genre_id`)
    REFERENCES `ride-pal`.`genres` (`genre_id`),
  CONSTRAINT `fk_playlists_has_genres_playlists1`
    FOREIGN KEY (`playlists_playlist_id`)
    REFERENCES `ride-pal`.`playlists` (`playlist_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ride-pal`.`tracks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`tracks` (
  `track_id` INT(20) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(250) NOT NULL,
  `link` VARCHAR(250) NOT NULL,
  `duration` INT(30) NOT NULL,
  `ranked` INT(250) NOT NULL,
  `preview_url` VARCHAR(250) NOT NULL,
  `artist_id` INT(20) NOT NULL,
  `genre_id` INT(20) NOT NULL,
  `album_id` INT(20) NOT NULL,
  PRIMARY KEY (`track_id`),
  UNIQUE INDEX `track_id_UNIQUE` (`track_id` ASC) VISIBLE,
  INDEX `fk_tracks_artists1_idx` (`artist_id` ASC) VISIBLE,
  INDEX `fk_tracks_genres1_idx` (`genre_id` ASC) VISIBLE,
  INDEX `fk_tracks_album1_idx` (`album_id` ASC) VISIBLE,
  CONSTRAINT `fk_tracks_album1`
    FOREIGN KEY (`album_id`)
    REFERENCES `ride-pal`.`albums` (`album_id`),
  CONSTRAINT `fk_tracks_artists1`
    FOREIGN KEY (`artist_id`)
    REFERENCES `ride-pal`.`artists` (`artist_id`),
  CONSTRAINT `fk_tracks_genres1`
    FOREIGN KEY (`genre_id`)
    REFERENCES `ride-pal`.`genres` (`genre_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8108
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `ride-pal`.`playlists_tracks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ride-pal`.`playlists_tracks` (
  `playlists_tracks_id` INT(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` INT(11) NOT NULL,
  `track_id` INT(11) NOT NULL,
  PRIMARY KEY (`playlists_tracks_id`),
  INDEX `fk_playlists_has_tracks_tracks1_idx` (`track_id` ASC) VISIBLE,
  INDEX `fk_playlists_has_tracks_playlists1_idx` (`playlist_id` ASC) VISIBLE,
  CONSTRAINT `fk_playlists_has_tracks_playlists1`
    FOREIGN KEY (`playlist_id`)
    REFERENCES `ride-pal`.`playlists` (`playlist_id`),
  CONSTRAINT `fk_playlists_has_tracks_tracks1`
    FOREIGN KEY (`track_id`)
    REFERENCES `ride-pal`.`tracks` (`track_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 400
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
